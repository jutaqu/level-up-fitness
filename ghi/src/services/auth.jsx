export async function login(baseUrl, username, password) {
    const url = `${baseUrl}/token`
    const form = new FormData()
    form.append('username', username)
    form.append('password', password)
    const response = await fetch(url, {
        method: 'POST',
        credentials: 'include',
        body: form,
    })
    if (!response.ok) {
        throw Error('Username or password invalid. Please try again.')
    }
    const data = await response.json()
    if (data.access_token) {
        return data.access_token
    } else {
        throw Error('Failed to get token after login.')
    }
}

export async function signup(accountData) {
    const baseUrl = import.meta.env.VITE_API_HOST_FROM_ENV
    if (!baseUrl) {
        throw Error('VITE_API_HOST_FROM_ENV is not set.')
    }
    const response = await fetch(
        `${import.meta.env.VITE_API_HOST_FROM_ENV}/api/accounts`,
        {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify(accountData),
            headers: {
                'Content-Type': 'application/json',
            },
        }
    )
    if (!response.ok) {
        throw Error(
            'Failed to create account. Please try a new username or email address'
        )
    }
}
