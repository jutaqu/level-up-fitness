import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import '../css/HealthData.css'

const API_HOST = import.meta.env.VITE_API_HOST_FROM_ENV

if (!API_HOST) {
    throw new Error('VITE_API_HOST is not defined')
}

export default function HealthData() {
    const navigate = useNavigate()
    const [userData, setUserData] = useState({})
    const [userHealthData, setUserHealthData] = useState({})

    const getHealthData = async () => {
        const url = `${API_HOST}/api/health_data`
        const fetchConfig = {
            method: 'get',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(url, fetchConfig)

        if (response.ok) {
            const data = await response.json()
            if (data[0]) {
                setUserHealthData(data[data.length - 1])
            } else {
                navigate('/healthdata/new')
            }
        }
    }

    const getUserData = async () => {
        const url = `${API_HOST}/token`
        const response = await fetch(url, {
            credentials: 'include',
        })
        if (response.ok) {
            const data = await response.json()
            if (data) {
                setUserData(data.account)
                getHealthData()
            } else {
                navigate('/')
            }
        }
    }

    const navigateCreateHealthData = async () => {
        navigate('/healthdata/new')
    }

    useEffect(() => {
        getUserData()
    }, [])

    return (
        <div>
            <label className="fs-1 fw-bold title">
                Hello, {userData.username}!
            </label>
            <p className="subtitle">
                Here is your most recently updated Health Data
            </p>
            <div className="card mx-auto" style={{ width: '60rem' }}>
                <table className="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Date Updated</th>
                            <th scope="col">Height</th>
                            <th scope="col">Weight</th>
                            <th scope="col">Age</th>
                            <th scope="col">Goal</th>
                            <th scope="col">Fitness Level</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{userHealthData.health_data_date}</td>
                            <td>{userHealthData.height}</td>
                            <td>{userHealthData.weight}</td>
                            <td>{userHealthData.age}</td>
                            <td>{userHealthData.goal}</td>
                            <td>{userHealthData.level}</td>
                        </tr>
                    </tbody>
                </table>
                <button
                    className="btn health-button button-pad"
                    onClick={() => {
                        navigateCreateHealthData()
                    }}
                >
                    Update my health Information
                </button>
            </div>
        </div>
    )
}
