## February 9, 2024
Today,

The number one focus for me was getting some styling on to the logged in page. After, we worked together to complete the outstanding project requirements (ReadMe, etc.) and sync our code in main so that gitlab had our most up to date, completed project.

## February 8, 2024
Today,

The big project for today was trying to make the date selected in the calendar of the logged in page be saved and used as variable for the workout page so that when the user clicks 'take me to the workout for {dynamic date}, it takes the user there. There were also some outstanding items on the workoutpage we had to resolve. Such as creating the useEffect, clearing the form after submitted, getting the data to accept the user input as an integer. 


## February 7, 2024
Today,

I completed getting the dashboard to show the workout completed by the user for that day. I initially was trying to do a back-end request to retrieve the user Id but we ended up changing the route on the back-end to retrieve the user Id via 'authenticator.try_get_current_account_data' which allows us to provide the user Id on the backend, providing convenience as well as additional security since this is one less authentication data request going to the front end as well as provides an additional means of verification that the requester is the logged in user and authorized to view their data and their data only. We then decided to refactor all of our routes so that anything requiring the user Id was provided from the backend rather than manual insertion on the front-end. 


## February 6, 2024
Today,

I got the table to display the workout for the date specified by what the user selects on the calendar. Our current backend for that fetch requires the user id and the date. We are going to try to modify the route so that it does not require the user Id. 
Once this is done, I am going to work on Making the page look nicer. We are getting close to the deadline and will need to start wrapping things up if we want to deploy prior to the due date. The instructors have not decided if deployment will be part of the grade yet.


## February 5, 2024
Today, 

While I was trying to be ambitious, I was unable to get the 2 sync'ed calendar functionality I was hoping for. Instead I decided to scrap the day view calendar for a list that will dynamically populate based on a specific date that user selects on the monthly calendar view. I got it to show all of the exercises the user has done, now i just need to figure out how to filter it by the day selected.

The past 2 days were great experience in working with the provided documentation to utilize a library. Unfortunately, I have to agree with a lot of coders online that the documentation is pretty bad and wasn't very useful. 

## February 1, 2024
Today,

On the user logged in home page I completed the "easy route" which allows the user to select a day on the month calendar to go to the exercise add list. Now that I have a known good, I added a second calendar Day view at the bottom right of the page and am working on syncing the user interaction so that when the user selcts a day from the month calendar, the day calendar on the right goes to the same date and from the day view calendar if the user clicks it takes them to add exercises for that day. It has been tedious and trial and error but I am slowly making progress on it.


## January 31, 2024
Today,

We have gotten to a point where the Foundation of our app has been created and we can start branching out on different areas. Most of it was already done by Justin and Jon but I finished the user login function. I then started working on the User logged in Home page. As part of the user logged in home page, I had to get a calendar to display on the left side of the page and allow the functionality of clicking a specific day on the calendar so that we could display the exercises for that day.

## January 30, 2024
Today,

Out focus was getting a succssful user signup and login using the front end of our website. 

## January 29, 2024
Today,

We shifted away from UNITEST and focused on building our front end. Starting with the main page(logged out/sign up screen). It was a great way to remember how to implement HTML and CSS since it's been a while as well as using React.

## January 26, 2024

Today, we tried working on UNITEST some more. There wasn't much time allocated for today with lectures and social hack hour. We are still struggling with import issues that we can't get to resolve.

## January 25, 2024
Today was a research day. We didn't do much active coding for our app. We are a little ahead of schedule since we are already focused on our front-end. 
Today's studies focused on createBrowserRouter, Unit Tests,JS vs JSX, and how the Main, App, Routes, and Components all tie in together to create the front end path.

## January 24, 2024
Today,

We started working on our front-end. We dove straight in trying to watch instructor lessons and follow along to apply to our app. After watching the videos we realized that authorization merely added on to the pre-existing front-end paths and that our focus should be creating the front-end paths first. Things completed today, established Vite URL path, successfully imported bootstrap, established our routes for our createBrowserRouter, and created a Navbar.

Today was especially helpful in introducing hands-on learning with Vite and Redux React. I also learned more about docker compose and where npm install saves its installs so that other users don't have to (in the package-json). Docker-compose.yaml is for docker hub and containers, and npm is for node/JS

## January 23, 2024
Today,

We resolved the error handling for all of our paths, created issues for the features made so far, and completed our backend. Yay! Tomorrow we will begin on our front-end and authorization.

## January 22, 2024
Today,
We started working on our 'health-data' path. We completed the back end route for our health data. The front end for this route will be a little tricky as we have to tie in our health-data with an external api that we are going to use to return their BMI.

## January 19, 2024
Today, 
We finished the path for 'exercises' to include the GET and POST of the exercise list, as well as the GET, PUSH, and DELETE for individual exercises.

It was a great experience in troubleshooting our code and keeping our composure when we cannot figure out what's causing a bug. 

## January 18, 2024
Today,
We finished up authentication on the backend. We then started working on the route path for exercises. 

Today was beneficial in grasping how the migrations, models, queries, routers, and main files all tie in and interact with each other to complete a path. 


## January 17, 2024
Today, 
We made big strides in creating our user authentication and user creation. We are ending the day at the tail end of creating the authentication, and are trying to figure out how to apply this to Postgres db as all the examples provided use Mongo as the db.

Today was also beneficial in greatly increasing my confidence in the 'starting' of a project with regards to the docker compose file, dockerfile.dev, and the requirements.txt file and how they all tie in.

Tomorrow we seek to complete authentication and continue on other aspects of the backend.

## January 16, 2024

Today,
We forked and cloned our project, added the Instructor and SEIR's to our project, created our Journal files, and executed the steps mentioned in the 'Database Setup' page in learn. 

