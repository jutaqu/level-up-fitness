from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
    status,
)
from typing import List
from authenticator import authenticator
from queries.health_data import HealthDataRepository
from models.health_data_models import HealthDataOut, HealthDataIn


router = APIRouter()


@router.post("/api/health_data", response_model=HealthDataOut)
def create_health_data(
    health_data: HealthDataIn,
    repo: HealthDataRepository = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
):
    try:
        new_health_data = repo.create(
            user_id=account["id"], health_data=health_data
        )
    except Exception:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create health data",
        )
    return new_health_data


@router.get("/api/health_data", response_model=List[HealthDataOut])
def get_all(
    repo: HealthDataRepository = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> List[HealthDataOut]:
    try:
        pass

    except Exception:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot get health data",
        )
    return repo.get_all(user_id=account["id"])
